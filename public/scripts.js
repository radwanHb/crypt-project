$(document).ready(function() {
    const globalKey = "MySecretKey";

    function encryptMessage(message) {
        const encryptedMessage = CryptoJS.AES.encrypt(message, globalKey).toString();
        return encryptedMessage;
    }

    function decryptMessage(encryptedMessage) {
        const decryptedMessage = CryptoJS.AES.decrypt(encryptedMessage, globalKey).toString(CryptoJS.enc.Utf8);
        return decryptedMessage;
    }

    $("#sendMessageBtn").click(function() {
        const message = $("#sendMessage").val();
        const encryptedMessage = encryptMessage(message);


        $.ajax({
            type: "POST",
            url: "/api/send",
            data: { message: encryptedMessage },
            success: function() {
                $("#sendMessage").val('');
                showCoaster("Message sent successfully!");
            },
            error: function() {
                showCoaster("Error sending message.");
            }
        });
    });

    $("#getMessageBtn").click(function() {

        $.ajax({
            type: "GET",
            url: "/api/get",
            success: function(data) {
                console.log(data);
                const decryptedMessage = decryptMessage(data);
                $("#getMessage").val(decryptedMessage);
            },
            error: function() {
                showCoaster("Error fetching message.");
            }
        });




    });

    function showCoaster(message) {
        $("#coaster").text(message).fadeIn(400).delay(2000).fadeOut(400);
    }
});
