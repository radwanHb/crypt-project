<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Secure Messaging</title>
    <link rel="stylesheet" href={{asset('styles.css')}}>
</head>
<body>
<div class="container">
    <h1>Secure Messaging</h1>
    <div class="message-container">
        <input type="text" id="getMessage" placeholder="Enter Message">
        <button id="getMessageBtn">Get Message</button>
    </div>
    <div class="message-container">
        <input type="text" id="sendMessage" placeholder="Enter Message">
        <button id="sendMessageBtn">Send Message</button>
    </div>
    <div id="coaster" class="coaster"></div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.1.1/crypto-js.js"></script>
<script src={{asset('scripts.js')}}></script>
</body>
</html>
