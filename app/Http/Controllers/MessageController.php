<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function send()
    {
        Message::create([
            'body' => request('message')
        ]);
    }

    public function get()
    {
       return Message::latest()->first()->body;
    }
}
